# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelSQL, ModelView
from trytond.pool import PoolMeta


class Brand(ModelSQL, ModelView):
    'Product Brand'
    __name__ = 'product.brand'
    name = fields.Char('Name', required=True)


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'
    code = fields.Function(fields.Char('Code'), 'get_code')
    reference = fields.Char('Reference', select=True)
    brand = fields.Many2One('product.brand', 'Brand')
    country = fields.Char('Country')

    def get_code(self, name=None):
        if self.products:
            return self.products[0].code

    @classmethod
    def search_code(cls, name, clause):
        field_name = 'products.code'
        return [(field_name, clause[1], clause[2])]

    @classmethod
    def search_rec_name(cls, name, clause):
        domain = super(Template, cls).search_rec_name(name, clause)
        return domain + [('reference', clause[1], clause[2])]


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'

    @classmethod
    def __setup__(cls):
        super(Product, cls).__setup__()

    @classmethod
    def search_rec_name(cls, name, clause):
        domain = super(Product, cls).search_rec_name(name, clause)
        return domain + [('reference', clause[1], clause[2])]
