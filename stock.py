# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class MoveByProductStart(metaclass=PoolMeta):
    'Move By Product Start'
    __name__ = 'stock_co.move_by_product.start'
    brand = fields.Many2One('product.brand', 'Brand')
